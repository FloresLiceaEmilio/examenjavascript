// Función para generar una edad aleatoria entre min y max
const btnGenerar = document.getElementById('btnGenerar');
btnGenerar.addEventListener('click', function(){
    function generarEdadAleatoria(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    // Array para almacenar las edades de las 100 personas
    const edades = [];
    
    // Generar edades aleatorias y almacenarlas en el array
    for (let i = 0; i < 100; i++) {
        const edad = generarEdadAleatoria(1, 90); // Rango de edades de 1 a 90 años
        edades.push(edad);
    }
    
    // Mostrar las edades en el label
    const resultadoLabel = document.getElementById('resultado');
    resultadoLabel.textContent = "Edades: " + edades.join(', ');
});

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function(){
   document.getElementById('resultado').textContent = "";

})




//Boton para calcular el total a pagar

const btnCalcular = document.getElementById('btnCalcular');



btnCalcular.addEventListener('click', function(){
    
const tipoViaje = document.getElementById('tipoViaje').value;
const precio = parseFloat(document.getElementById('precio').value);
let subtotal, impuesto, total;
        
        if(tipoViaje == '1'){
            subtotal = precio;
            impuesto = precio * 0.16;
            total = subtotal + impuesto;
        }
        else{
            subtotal = precio * (precio * 0.80);
            impuesto = precio * 0.16;
            total = subtotal+impuesto;
        }

        document.getElementById('subtotal').value = subtotal;
        document.getElementById('impuesto').value = impuesto;
        document.getElementById('total').value = total;

});

const btnLimpiar = document.getElementById('btnLimpiar');
btnLimpiar.addEventListener('click', function(){

    document.getElementById('numBoleto').value = "";
    document.getElementById('nombre').value = "";
    document.getElementById('destino').value = "";
    document.getElementById('precio').value = "";
    document.getElementById('subtotal').value = "";
    document.getElementById('impuesto').value = "";
    document.getElementById('total').value = "";
});